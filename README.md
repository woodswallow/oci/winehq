# WineHQ

Wine based on WineHQ environment: Run interactive or automated Windows applications inside containers

There are images generated using different Ubuntu base versions:

- Production registry (stable images, persistent, immutable)
  - **latest**: Last tagged image, ensures a tested and working condition
    - **18.04** as `registry.gitlab.com/woodswallow/hub/winehq:latest-ubuntu18.04`
    - **20.04** as `registry.gitlab.com/woodswallow/hub/winehq:latest-ubuntu20.04`
    - **20.10** as `registry.gitlab.com/woodswallow/hub/winehq:latest-ubuntu20.10`
- Development registry (latest images, maybe inconsistent, maybe not immutable)
  - **master**: Last image from master branch, assumes a working condition
    - **18.04** as `registry.gitlab.com/woodswallow/oci/winehq:master-ubuntu18.04`
    - **20.04** as `registry.gitlab.com/woodswallow/oci/winehq:master-ubuntu20.04`
    - **20.10** as `registry.gitlab.com/woodswallow/oci/winehq:master-ubuntu20.10`

To connect using a VNC client:

```bash
docker run \
  --rm \
  -it \
  -p 127.0.0.1:5900:5900/tcp \
  registry.gitlab.com/woodswallow/hub/winehq:latest-ubuntu20.04
```

Then connect to `127.0.0.1:5900`.

To connect using the noVNC HTML5 VNC client:

```bash
docker run \
  --rm \
  -it \
  -p 127.0.0.1:6080:6080/tcp \
  registry.gitlab.com/woodswallow/hub/winehq:latest-ubuntu20.04
```

Then open [http://127.0.0.1:6080](http://127.0.0.1:6080).

# Base image to be used
ARG OCI_FROM_IMAGE_FQN
FROM ${OCI_FROM_IMAGE_FQN}

# Using 'novnc' base image, the following environment vars are already defined:
# - OCI_VARIANT_TITLE
# - OCI_USER_UID
# - OCI_USER_GID
# - DISPLAY
# - XVFB_RES
# - X11VNC_PORT
# - NOVNC_PORT
# - NOVNC_CMD

# Ensure enough rights
USER root

# Install block
RUN set -ex; \
  export DEBIAN_FRONTEND="noninteractive"; \
  # Mandatory update
  apt-get -y update; \
  # Install xvfb, x11vnc and supervisor with no confirmations
  apt-get -y install \
    software-properties-common \
    apt-transport-https \
  ; \
  # Wine and WineHQ needs i386, always
  dpkg --add-architecture i386; \
  wget -nv https://dl.winehq.org/wine-builds/winehq.key; \
  apt-key add winehq.key; \
  apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/; \
  if [ "${OCI_VARIANT_TITLE}" = "ubuntu18.04" ]; \
  then \
    # FAudio requirement when wineHQ and 18.04
    add-apt-repository ppa:cybermax-dexter/sdl2-backport; \
  fi; \
  apt-get -y update; \
  apt-get -y install --install-recommends \
    winehq-stable \
  ; \
  # Clean stage
  apt-get -y remove \
    software-properties-common \
    apt-transport-https \
  ; \
  apt-get -y clean; \
  apt-get -y autoclean; \
  apt-get -y autoremove; \
  rm -rf /var/lib/apt/lists/*; \
  rm winehq.key; \
  # Install winetricks
  wget -nv -O /usr/bin/winetricks \
    https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks; \
  chmod +x /usr/bin/winetricks;

# Global environment
ENV \
  # Wine will not ask for mono/gecko
  WINEDLLOVERRIDES="${WINEDLLOVERRIDES:-mscoree,mshtml=,winebrowser.exe=}" \
  # Disable all debug information
  WINEDEBUG="-all" \
  # Force 32bits usage (smaller image)
  WINEARCH="win32" \
  # Wine folder
  WINEPREFIX="/home/user/.wine"

# Switch to the user and its home folder
USER user:group
WORKDIR "/home/user"

# Default to supervisord (child will owns PID)
# NOTE: Inherited CMD does not need to be executed. Enable it only if custom
#CMD [ "bash", "-c", "${NOVNC_CMD}" ]
